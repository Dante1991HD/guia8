#include <time.h>
#include <stdlib.h>
#include "Ordenar.h"

using namespace std;

/* Constructor por defecto */
Ordenar::Ordenar(){
}


/* Método que aplica el algoritmo de orden seleccion */
void Ordenar::seleccion(int arreglo[], int numero){
    // Variables a utilizar dentro del metodo seleccion
    int i, menor, k, j;
    // Se debe empezar buscando el numero más pequeño
    for(i = 0; i<=numero-2;i++){
        // Guardamos el numero del arreglo que se va iterando en menor
        menor = arreglo[i];
        k = i;
        // Buscamos el segundo elemento más pequeño
        for(j = i+1; j<=numero-1; j++){
            // Si el que se va iterando es menor al elemento de arreglo se reemplaza y se intercambia posición
            if(arreglo[j] < menor){
                menor = arreglo[j];
                k = j;
            }
        }
        // Se reemplazan las posiciones
        arreglo[k] = arreglo[i];
        // El arreglo en la pos[i] queda como el menor encontrado
        arreglo[i] = menor;
    }  
}

/* Método auxiliar del algoritmo quicksort */
void reduce_quicksort(int arreglo[], int inicio, int fin, int &pos){
    // Variables a usar dentro del metodo
    int izq, der, aux;
    bool band = true;
    // Se colocan valores
    izq = inicio;
    der = fin;
    pos = inicio;
    while(band){
        // Se compara el valor actual con los elementos desde la derecha hacia la izquierda
        while((arreglo[pos] <= arreglo[der]) && (pos != der)){
            der--;
        }
        // No hay elementos más pequeños al actual que indica la posición
        if(pos == der){
            band = false;
        }
        else{
            // Intercambio de las posiciones
            aux = arreglo[pos];
            arreglo[pos] = arreglo[der];
            arreglo[der] = aux;
            pos = der;
            // Ahora comparamos el valor actual con los elementos desde izquierda a derecha
            while((arreglo[pos] >= arreglo[izq]) && (pos!= izq)){
                izq++;
            }
            // Si no hay elemento mayores al actual 
            if(pos == izq){
                band = false;
            }
            else{
                // Intercambio en las posiciones
                aux = arreglo[pos];
                arreglo[pos] = arreglo[izq];
                arreglo[izq] = aux;
                pos = izq;
            }
        }
    }

}


/* Método que aplica el algoritmo de orden quicksort */
void Ordenar::quicksort(int arreglo[], int numero){
    // Variables a utilizar dentro del metodo quicksort
    int tope, inicio, fin, pos;
    int pila_menor[numero];
    int pila_mayor[numero];
    // Se definen los topes
    tope = 0;
    pila_menor[tope] = 0;
    pila_mayor[tope] = numero-1;
    // El ciclo seguira ese condicion
    while(tope >=0){
        // Se toman los valores de las pilas
        inicio = pila_menor[tope];
        fin = pila_mayor[tope];
        tope--;
        // Llamamos al metodo reduce 
        reduce_quicksort(arreglo, inicio, fin, pos);
        // Ordenamos el menor
        if(inicio < pos-1){
            tope++;
            pila_menor[tope] = inicio;
            pila_mayor[tope] = pos - 1;
        }
        // Ordenamos el mayor
        if(fin > pos+1){
            tope++;
            pila_menor[tope] = pos+1;
            pila_mayor[tope] = fin;
        }
    }
    // Una vez finalizado el metodo, si el mostrar es verdadero se imprimira el ordenado
}

/* Método para imprimir los arreglos */
void Ordenar::imprimir(int arreglo[], int numero){
    // Se imprime el arreglo
    for(int i=0;i<numero;i++){
        cout << "a[" << i << "]=" << arreglo[i] << (i != numero ? " " : "");
    }
    cout << endl << endl;
}
