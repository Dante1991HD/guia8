#include <iostream>
#include <time.h>
#include <stdlib.h>

#ifndef ORDENAR_H
#define ORDENAR_H

using namespace std;

// Creacion de la clase ordenar
class Ordenar{
    // No hay atributos privados
    private:

    public:
        // Constructor por defecto
        Ordenar();
        // Metodo para el ordenamiento por seleccion
        void seleccion(int arreglo[], int numero);
        // Metodo de reduce del quicksort
        // Metodo para el ordenamiento por quicksort
        void quicksort(int arreglo[], int numero);
        // Metodo para imprimir arreglos
        void imprimir(int arreglo[], int numero);

};
#endif
