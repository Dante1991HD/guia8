#include <time.h>
#include <stdlib.h>
#include <chrono>
#include "Ordenar.h"
/* Se definen limites para la creación de datos aleatorios, con el fin de 
evitar que se genere un sobre esfuerzo en el hardware */
#define LIMITE_SUPERIOR 1000000
#define LIMITE_INFERIOR -1000000

/* Función que genera una salida con los datos del arreglo sin orden */
void imprimir_arreglo(int arreglo[], int numero){
    for(int i=0;i<numero;i++){
        cout << "a[" << i << "]=" << arreglo[i] << (i != numero ? " " : "");
    }
    cout << endl;
}


/* Función principal del sistema */
int main(int argc, char* argv[]){
    system("clear");
    // Variables a utilizar dentro del codigo
    int numero = 0;
    int i=0;
    int elemento=0;
    bool mostrar;
    string letra;
    // Ocupamos la libreria de chrono para generar los 2 tiempos para el ordenamiento
    chrono::duration<float> tiempo1, tiempo2;
    // Nuestra clase ordenar
    Ordenar *ordenar;
    // Si se entregan los 3 parametros
    if(argc == 3){
        // Se captura el numero
        numero = atoi(argv[1]);
        // Capturamos la letra acompañada
        letra = argv[2];
        // Validamos de que el numero que este en el rango
		if (numero > LIMITE_SUPERIOR || numero < 2){
			cout << "Numero invalido para la ejecucion del programa" << endl;
			cout << "-- Debe ser menor o igual a 1.000.000 y almenos 2--" << endl;
			return 1;
		}
    }else{
        cout << "-- Se necesitan 2 parametros -- " << endl;
        cout << "-- refiera a README.md -- " << endl;
        cout << "--ejemplo: ./programa 10 s -- " << endl;
		return 1;
    }

    // Declaracion de los arreglos
    int lista[numero];
    int seleccion[numero];
    int quick[numero];
    srand(time(NULL));

    // Generacion del arreglo aleatorio
    for(i=0; i<numero; i++){
        elemento = (rand() % LIMITE_SUPERIOR) + LIMITE_INFERIOR;
        lista[i] = elemento; 
    }

    // Se imprime el arreglo que se genero
    cout << "\nArreglo aleatorio generado" << endl;
    cout << "----------------------------------" << endl;
    imprimir_arreglo(lista, numero);
    
/* Pasamos la lista de datos aleatorios generada a los arreglos para cada algoritmo de orden */
    for(int i=0;i<numero;i++){
        seleccion[i] = lista[i];
        quick[i] = lista[i];
    }
    mostrar = letra == "s";

    // Metodo seleccion
    // Ocupamos la libreria de chrono para tomar el tiempo
    auto tiempo_inicial = chrono::high_resolution_clock::now();
    // Ordenamos el arreglo con el metodo seleccion 
    ordenar->seleccion(seleccion, numero);
    // Tomamos el tiempo luego de ordenar
    auto tiempo_final = chrono::high_resolution_clock::now();
    // Guardamos el tiempo
    tiempo1 = tiempo_final-tiempo_inicial;

    // Metodo quicksort
    // Ocupamos nuevamente la librería chrono
    tiempo_inicial = chrono::high_resolution_clock::now();
    ordenar->quicksort(quick, numero);
    tiempo_final = chrono::high_resolution_clock::now();
    // Guardamos el tiempo
    tiempo2 = tiempo_final-tiempo_inicial;
    /* Salida con los datos de tiempo para cada algoritmo de ordenamiento*/
    cout << endl;
    cout << "----------------------------------" << endl;
    cout << "Metodo    |Tiempo" << endl;
    cout << "----------------------------------" << endl;
    cout << "Seleccion |" << tiempo1.count() << " milisegundos" << endl;
    cout << "----------------------------------" << endl;
    cout << "Quicksort |" << tiempo2.count() << " milisegundos" << endl;
    cout << "----------------------------------" << endl;
    /* Si el usuario pidió que se muestren las salidas ordenadas se pasa a lo siguiente */
    if(mostrar){
        cout << "Selection:";
        ordenar->imprimir(seleccion, numero);
        cout << "QuickSort:";
        ordenar->imprimir(quick, numero);
    }

    return 0;
}
