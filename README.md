# Guía VIII UIII

## Descripción del programa 
El programa genera un arreglo de números aleatorios de tamaño designado por el usuario, luego se le aplica 2 algoritmos de ordenamiento de manera paralela, seleccion y quicksort, el programa mostrará en pantalla por defecto el arreglo inicial y el tiempo que toma en completar cada orden por separado, además el usuario puede elegir, al momento de correr el programa, si mostrar el contenido de los vectores en que se guardan los resultados de cada algoritmo.

## Requerimientos para utilizar el programa
* Sistema operativo Linux
* Compilador make
En caso de no contar con make:

### ¿Como instalar make?
Para instalar make, Ud. debe acceder a una terminal dentro del sistema operativo linux e ingresar el siguiente comando:
```
sudo apt-get install make
```
Luego ingresar clave de usuario el sistema procederá a descargar e instalar el compilador. Una vez termine la descarga, su compilador estará listo para usar.

## Compilación y ejecución del programa
* Abrir una terminal en el directorio donde se ubican los archivos del programa e ingresar el siguiente comando.
```
make
```
* Luego de completada la compilación utilice el siguiente comando.
```
./programa #Número entero# #s para mostrar el verctor n para no mostrar#
```
* Es de vital importancia que se agrege un numero entero desde la consola y que este sea mayor a 2 e ingrese "s" o "n" como en el siguiente ejemplo.
```
./programa 5 s
```
## Salidas

* El programa generará una salida como la siguiente
```
Arreglo aleatorio generado
----------------------------------
a[0]=767564 a[1]=309326 a[2]=-19554 a[3]=584958 a[4]=341453 

----------------------------------
Metodo    |Tiempo
----------------------------------
Seleccion |1.081e-06 milisegundos
----------------------------------
Quicksort |1.408e-06 milisegundos
----------------------------------
Selection:a[0]=-19554 a[1]=309326 a[2]=341453 a[3]=584958 a[4]=767564 

QuickSort:a[0]=-19554 a[1]=309326 a[2]=341453 a[3]=584958 a[4]=767564 
```

## Autor
* Dante Aguirre
* Correo: daguirre12@alumnos.utalca.cl
